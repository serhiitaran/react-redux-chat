export const getData = async url => {
  const res = await fetch(url);
  if (!res.ok) {
    throw new Error(`Received ${res.status}`);
  }
  return await res.json();
};
