import { getData } from '../helpers/apiHelper';
import { createMessages } from '../helpers/chatHelper';

const API_URL = 'https://edikdolynskyi.github.io/react_sources/messages.json';

const getMessages = async url => {
  const data = await getData(API_URL);
  const messages = createMessages(data);
  return messages;
};

const getUser = () => {
  return {
    user: 'Ben',
    avatar: 'https://www.aceshowbiz.com/images/photo/tom_pelphrey.jpg',
    userId: '5328dba1-1b8f-11e8-9629-c7eca82aa7bd'
  };
};

const getChatName = () => 'Redux chat';

const getChatData = async () => {
  const messages = await getMessages(API_URL);
  const user = getUser();
  const chatName = getChatName();
  return { chatName, messages, user };
};

export default getChatData;
